# GELlulose
Like Cellulose, but differnt!

## Tools
This repository contains some tools to work with GEL files, which optionally can
be run in Docker.

### Docker
To create a local docker container for use, calling the 'docker_run.sh' script
is enough. Tagging and pushing to a registry manually is not done by the script
as this is normally handled by the CI. For now the CI will only push docker
images to the registry on the master branch.

### Natively
Docker is not required to run these scripts, but it makes it easier without having
to install all dependencies. To check if the used system is compatible, the script
`buildenv_check.sh` can be run.
