#!/bin/sh
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
#
# SPDX-License-Identifier: AGPL-3.0-or-later

set -eu

CMDS="
    [
    command
    echo
    exit
    getopts
    printf
    rm
    set
    shift
    test
"


usage()
{
    echo "Usage: ${0} [OPTIONS]"
    echo "A simple test script to verify the environment has all required tools."
    echo "    -h  Print usage"
}

check_requirements()
{
    for cmd in ${CMDS}; do
        if ! test_result="$(command -V "${cmd}")"; then
            test_result_fail="${test_result_fail:-}${test_result}\n"
        else
            test_result_pass="${test_result_pass:-}${test_result}\n"
        fi
    done

    echo "Passed tests:"
    # As the results contain \n, we expect these to be interpreted.
    # shellcheck disable=SC2059
    printf "${test_result_pass:-none\n}"
    echo
    echo "Failed tests:"
    # shellcheck disable=SC2059
    printf "${test_result_fail:-none\n}"
    echo

    if [ -n "${test_result_fail:-}" ]; then
        echo "Self-test failed, missing dependencies."
        exit 1
    fi
}

main()
{
    while getopts ":h" options; do
        case "${options}" in
        h)
            usage
            exit 0
            ;;
        :)
            echo "Option -${OPTARG} requires an argument."
            exit 1
            ;;
        ?)
            echo "Invalid option: -${OPTARG}"
            exit 1
            ;;
        esac
    done
    shift "$((OPTIND - 1))"

    check_requirements

    echo "All Ok"
}

main "${@}"

exit 0
