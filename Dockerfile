# SPDX-License-Identifier: AGPL-3.0-or-later
#
# Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>

ARG ALPINE_VERSION="latest"
ARG TARGET_ARCH="library"

FROM registry.hub.docker.com/${TARGET_ARCH}/alpine:${ALPINE_VERSION}

LABEL Maintainer="Olliver Schinagl <oliver@schinagl.nl>"

RUN \
    rm -rf "/var/cache/apk/"*

COPY "./dockerfiles/buildenv_check.sh" "/test/buildenv_check.sh"
